# this below Code is used check the given mobile number is registered with whatsapp or not 
import requests
import csv
import re


#example url https://api.ultramsg.com/{instance ID}/contacts/check
url = ""
token=""
data=[["number","status"]]

def outputfun(data):
    with open('output.csv', 'w') as f:
        csvwriter = csv.writer(f)
        csvwriter.writerows(data)

with open('numbers.csv', mode ='r') as file:
    csvFile = csv.reader(file)
    for number in csvFile:
        r=re.fullmatch('[5-9][0-9]{9}',number[0])
        if r!=None:
           querystring = {
           "token": token,
           "chatId": "+91{}".format(number[0]),
           "nocache": ""
           }
           print("hello")
           headers = {'content-type': 'application/x-www-form-urlencoded'}
           response = requests.request("GET", url, headers=headers, params=querystring)
           new=response.json()
           subdata=[number[0],new['status']]
           print(subdata)
           data.append(subdata)
           print(new['status'])
        else: 
           subdata=[number[0],"unknown"]
           data.append(subdata)
outputfun(data)
print(data)

#ref
#https://ultramsg.com/
#https://docs.ultramsg.com/api/get/contacts/check